var TrojanskyUiKit = /** @class */ (function () {
    function TrojanskyUiKit() {
        this.datePicker = new /** @class */ (function () {
            function datePicker() {
                this.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                this.weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
            }
            datePicker.prototype.init = function (format) {
                if (format === void 0) { format = 'YYYY-MM-DD'; }
                this.format = format;
                this.addElement();
                this.addListener();
            };
            datePicker.prototype.setMonths = function (months) {
                if (months.length != 12) {
                    console.warn('Date picker requires twelve months to be added');
                    return false;
                }
                this.months = months;
                this.addElement();
                return true;
            };
            datePicker.prototype.setWeekDays = function (weekDays) {
                if (weekDays.length != 7) {
                    console.warn('Datepicker requires seven days of the week ');
                    return false;
                }
                this.weekDays = weekDays;
                this.addElement();
                return true;
            };
            datePicker.prototype.addElement = function () {
                var datepicker = document.querySelector('#datepicker');
                if (datepicker) {
                    datepicker.remove();
                }
                var htmlDaysOfWeek = '';
                this.weekDays.forEach(function (item, i) {
                    htmlDaysOfWeek += '<td>' + item.substring(0, 3) + '</td>';
                });
                document.body.innerHTML += "\n                <div id=\"datepicker\" class=\"datepicker-hide\">\n                    <div id=\"datepicker-info\">\n                    </div>\n                    <table>\n                        <thead>\n                        <tr>" + htmlDaysOfWeek + "</tr>\n                        </thead>\n                        <tbody id=\"datepicker-calendar\">\n                        </tbody>\n                    </table>\n                </div>\n            ";
            };
            datePicker.prototype.addListener = function () {
                var _this = this;
                document.addEventListener("click", function (e) {
                    var target = e.target;
                    if (e.target == document.getElementById('datepicker') || target.closest('#datepicker') == document.getElementById('datepicker')) {
                        if (target.classList.contains('select-date')) {
                            _this.lastDatepicker.value = target.getAttribute('data-value');
                            document.getElementById('datepicker').classList.add('datepicker-hide');
                        }
                        else if (target.classList.contains('change-datepicker')) {
                            var month = parseInt(target.getAttribute('data-month'));
                            var year = parseInt(target.getAttribute('data-year'));
                            _this.runDatepicker(e, month, year);
                        }
                    }
                    else if (!target.classList.contains('datepicker')) {
                        document.getElementById('datepicker').classList.add('datepicker-hide');
                    }
                    if (target.classList.contains('datepicker')) {
                        _this.runDatepicker(e);
                        var datepicker = document.getElementById('datepicker');
                        datepicker.classList.remove('datepicker-hide');
                        var rect = target.getBoundingClientRect();
                        datepicker.style.transform = 'translate(' + (rect.left + window.scrollX + 4) + 'px, ' + (rect.top + window.scrollY + 50) + 'px)';
                        _this.lastDatepicker = e.target;
                    }
                });
            };
            datePicker.prototype.runDatepicker = function (e, month, year) {
                if (month === void 0) { month = 0; }
                if (year === void 0) { year = 0; }
                if (month == -1) {
                    month = 11;
                    year--;
                }
                if (month == 12) {
                    month = 0;
                    year++;
                }
                if (!month && !year) {
                    month = new Date().getMonth();
                    year = new Date().getFullYear();
                    if (e.target.value.split('-').length == 3) {
                        var date = e.target.value.split('-');
                        var dateFormat = this.format.split('-');
                        month = parseInt(date[dateFormat.indexOf('MM')]) - 1;
                        year = parseInt(date[dateFormat.indexOf('YYYY')]);
                    }
                }
                var afterFirstDay = false;
                var day = 0;
                document.getElementById('datepicker-info').innerHTML = "\n                <div class=\"change-datepicker change-datepicker-prev\" data-year=\"" + year + "\" data-month=\"" + (month - 1) + "\"><</div> \n                <p>" + this.months[month] + " " + year + "</p>\n                <div class=\"change-datepicker change-datepicker-next\" data-year=\"" + year + "\" data-month=\"" + (month + 1) + "\">></div>\n            ";
                document.getElementById('datepicker-calendar').innerHTML = '<tr></tr>';
                for (var i = 0; day < this.getNumberOfDays(year, month); i++) {
                    if (i % 7 == 0 && i !== 1) {
                        document.getElementById('datepicker-calendar').innerHTML += '<tr></tr>';
                    }
                    var tr = document.getElementById('datepicker-calendar').getElementsByTagName('TR');
                    if (i == this.getFirstDay(year, month)) {
                        afterFirstDay = true;
                    }
                    var date = this.format;
                    date = date.replace('YYYY', year.toString());
                    date = date.replace('MM', String("0" + (month + 1)).slice(-2));
                    date = date.replace('DD', String("0" + (day + 1)).slice(-2));
                    if (afterFirstDay) {
                        day++;
                        if (e.target.value == date) {
                            tr[tr.length - 1].innerHTML += '<td class="selected-date" data-value="' + date + '">' + day + '</td>';
                        }
                        else {
                            tr[tr.length - 1].innerHTML += '<td class="select-date" data-value="' + date + '">' + day + '</td>';
                        }
                    }
                    else {
                        tr[tr.length - 1].innerHTML += '<td></td>';
                    }
                }
            };
            datePicker.prototype.getNumberOfDays = function (year, month) {
                var date = new Date(year, month);
                return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
            };
            datePicker.prototype.getFirstDay = function (year, month) {
                var date = new Date(year, month);
                return new Date(date.getFullYear() + "-" + (date.getMonth() + 1) + "-01").getDay();
            };
            return datePicker;
        }());
        this.animations = new /** @class */ (function () {
            function animations() {
                this.elements = {};
            }
            animations.prototype.init = function () {
                this.addListener();
                this.reloadElements();
                this.makeAnimation();
            };
            animations.prototype.reloadElements = function () {
                this.elements = document.querySelectorAll('[anim]');
                this.elements.forEach(function (item, i) {
                    item.classList.remove('anim-run');
                });
            };
            animations.prototype.addListener = function () {
                var _this = this;
                window.addEventListener('scroll', function (e) {
                    _this.makeAnimation();
                });
            };
            animations.prototype.makeAnimation = function () {
                if (this.elements.length) {
                    this.elements.forEach(function (item, i) {
                        if (!item.classList.contains('anim-run')) {
                            if (window.scrollY > (item.offsetTop - window.innerHeight)) {
                                var cooldown = typeof item.attributes['anim-cooldown'] != 'undefined' ? parseInt(item.attributes['anim-cooldown'].value) : 100;
                                item.classList.add('anim-prepare');
                                setTimeout(function () {
                                    item.classList.remove('anim-prepare');
                                    item.classList.add('anim-run');
                                }, cooldown);
                            }
                        }
                    });
                }
            };
            return animations;
        }());
        this.parallax = new /** @class */ (function () {
            function parallax() {
                this.elements = {};
            }
            parallax.prototype.init = function () {
                this.addListener();
                this.reloadElements();
            };
            parallax.prototype.reloadElements = function () {
                this.elements = document.querySelectorAll('.parallax');
            };
            parallax.prototype.addListener = function () {
                var _this = this;
                window.addEventListener('scroll', function (e) {
                    if (_this.elements.length) {
                        _this.elements.forEach(function (item, i) {
                            var s = typeof item.attributes['parallax-speed'] != 'undefined' ? parseFloat(item.attributes['parallax-speed'].value) : 1;
                            var y = (window.pageYOffset * s) + (typeof item.attributes['parallax-top'] != 'undefined' ? parseFloat(item.attributes['parallax-top'].value) : 0);
                            item.style.backgroundPosition = '0 ' + y + 'px';
                        });
                    }
                });
            };
            return parallax;
        }());
        this.theme = new /** @class */ (function () {
            function theme() {
                this.addClass();
            }
            theme.prototype.addClass = function () {
                var cookies = document.cookie.split('; ');
                cookies.forEach(function (item, i) {
                    if (item.lastIndexOf('theme=', 0) === 0) {
                        var theme_1 = item.split('theme=')[1];
                        document.body.classList.add(theme_1 + '-theme');
                    }
                });
            };
            theme.prototype.setCookie = function (theme, days) {
                if (theme === void 0) { theme = 'light'; }
                if (days === void 0) { days = 30; }
                var date = new Date(), expire = '';
                date.setTime(date.getTime() + (1000 * 60 * 60 * 24 * days));
                expire = '; expires=' + date.toUTCString();
                document.cookie = 'theme=' + theme + expire + ';SameSite=Lax';
            };
            theme.prototype.setTheme = function (theme) {
                if (theme === void 0) { theme = 'light'; }
                var body = document.body;
                if (theme == 'dark') {
                    body.classList.remove('light-theme');
                    body.classList.add('dark-theme');
                }
                else if (theme == 'light') {
                    body.classList.add('light-theme');
                    body.classList.remove('dark-theme');
                }
                this.setCookie(theme);
            };
            return theme;
        }());
        this.mouseFollower = new /** @class */ (function () {
            function mouseFollower() {
            }
            mouseFollower.prototype.init = function () {
                this.mouseMoveListener();
                this.addElements();
                this.clickListeners();
            };
            mouseFollower.prototype.addElements = function () {
                var mf = document.createElement('div');
                var mfR = document.createElement('div');
                mf.className = 'mouse-follower';
                mfR.className = 'mouse-follower';
                mf.id = 'mouse-follower';
                mfR.id = 'mouse-follower-ring';
                document.body.prepend(mf);
                document.body.prepend(mfR);
            };
            mouseFollower.prototype.clickListeners = function () {
                window.addEventListener('mousedown', function (e) {
                    var elR = document.getElementById('mouse-follower-ring');
                    elR.classList.add('click');
                });
                window.addEventListener('mouseup', function (e) {
                    var elR = document.getElementById('mouse-follower-ring');
                    elR.classList.remove('click');
                });
            };
            mouseFollower.prototype.mouseMoveListener = function () {
                window.addEventListener('mousemove', function (e) {
                    var target = e.target;
                    var el = document.getElementById('mouse-follower');
                    var elR = document.getElementById('mouse-follower-ring');
                    setTimeout(function () {
                        el.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
                    }, 25);
                    setTimeout(function () {
                        elR.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
                    }, 100);
                    var tag = target.tagName;
                    if (tag == 'BUTTON' || tag == 'A') {
                        if (!(typeof target.attributes['href'] !== 'undefined' && target.attributes['href'].value == '#')) {
                            el.classList.add('grow');
                            elR.classList.add('grow');
                        }
                    }
                    else {
                        el.classList.remove('grow');
                        elR.classList.remove('grow');
                    }
                });
            };
            return mouseFollower;
        }());
    }
    return TrojanskyUiKit;
}());
var TUiKit = new TrojanskyUiKit();
//# sourceMappingURL=trojansky-ui-kit.js.map