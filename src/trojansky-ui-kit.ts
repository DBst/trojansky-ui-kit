class TrojanskyUiKit {

    datePicker = new class datePicker {

        format: string;
        lastDatepicker: any;
        months: Array<String> = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        weekDays: Array<String> = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

        init(format = 'YYYY-MM-DD') {
            this.format = format;
            this.addElement();
            this.addListener();
        }

        setMonths(months: Array<String>): boolean {
            if (months.length != 12) {
                console.warn('Date picker requires twelve months to be added');
                return false;
            }
            this.months = months;
            this.addElement();
            return true;
        }

        setWeekDays(weekDays: Array<String>): boolean {
            if (weekDays.length != 7) {
                console.warn('Datepicker requires seven days of the week ');
                return false;
            }
            this.weekDays = weekDays;
            this.addElement();
            return true;
        }

        private addElement() {
            const datepicker = document.querySelector('#datepicker');
            if (datepicker) {
                datepicker.remove();
            }
            let htmlDaysOfWeek = '';
            this.weekDays.forEach(function (item: string, i: number) {
                htmlDaysOfWeek += '<td>' + item.substring(0, 3) + '</td>';
            });
            document.body.innerHTML += `
                <div id="datepicker" class="datepicker-hide">
                    <div id="datepicker-info">
                    </div>
                    <table>
                        <thead>
                        <tr>` + htmlDaysOfWeek + `</tr>
                        </thead>
                        <tbody id="datepicker-calendar">
                        </tbody>
                    </table>
                </div>
            `;
        }

        private addListener() {
            document.addEventListener("click", (e) => {
                const target = e.target as HTMLAreaElement;
                if (e.target == document.getElementById('datepicker') || target.closest('#datepicker') == document.getElementById('datepicker')) {
                    if (target.classList.contains('select-date')) {
                        this.lastDatepicker.value = target.getAttribute('data-value');
                        document.getElementById('datepicker').classList.add('datepicker-hide');
                    } else if (target.classList.contains('change-datepicker')) {
                        let month = parseInt(target.getAttribute('data-month'));
                        let year = parseInt(target.getAttribute('data-year'));
                        this.runDatepicker(e, month, year);
                    }
                } else if (!target.classList.contains('datepicker')) {
                    document.getElementById('datepicker').classList.add('datepicker-hide');
                }
                if (target.classList.contains('datepicker')) {
                    this.runDatepicker(e);
                    let datepicker = document.getElementById('datepicker');
                    datepicker.classList.remove('datepicker-hide');
                    const rect = target.getBoundingClientRect();
                    datepicker.style.transform = 'translate(' + (rect.left + window.scrollX + 4) + 'px, ' + (rect.top + window.scrollY + 50) + 'px)';
                    this.lastDatepicker = e.target;
                }
            });
        }

        private runDatepicker(e: any, month: number = 0, year: number = 0) {
            if (month == -1) {
                month = 11;
                year--;
            }
            if (month == 12) {
                month = 0;
                year++;
            }
            if (!month && !year) {
                month = new Date().getMonth();
                year = new Date().getFullYear();
                if (e.target.value.split('-').length == 3) {
                    let date = e.target.value.split('-');
                    let dateFormat = this.format.split('-');
                    month = parseInt(date[dateFormat.indexOf('MM')]) - 1;
                    year = parseInt(date[dateFormat.indexOf('YYYY')]);
                }
            }
            let afterFirstDay = false;
            let day = 0;
            document.getElementById('datepicker-info').innerHTML = `
                <div class="change-datepicker change-datepicker-prev" data-year="` + year + `" data-month="` + (month - 1) + `"><</div> 
                <p>` + this.months[month] + ` ` + year + `</p>
                <div class="change-datepicker change-datepicker-next" data-year="` + year + `" data-month="` + (month + 1) + `">></div>
            `;
            document.getElementById('datepicker-calendar').innerHTML = '<tr></tr>';
            for (let i = 0; day < this.getNumberOfDays(year, month); i++) {
                if (i % 7 == 0 && i !== 1) {
                    document.getElementById('datepicker-calendar').innerHTML += '<tr></tr>';
                }
                let tr = document.getElementById('datepicker-calendar').getElementsByTagName('TR');
                if (i == this.getFirstDay(year, month)) {
                    afterFirstDay = true;
                }
                let date = this.format;
                date = date.replace('YYYY', year.toString());
                date = date.replace('MM', String("0" + (month + 1)).slice(-2));
                date = date.replace('DD', String("0" + (day + 1)).slice(-2));
                if (afterFirstDay) {
                    day++;
                    if (e.target.value == date) {
                        tr[tr.length - 1].innerHTML += '<td class="selected-date" data-value="' + date + '">' + day + '</td>';
                    } else {
                        tr[tr.length - 1].innerHTML += '<td class="select-date" data-value="' + date + '">' + day + '</td>';
                    }
                } else {
                    tr[tr.length - 1].innerHTML += '<td></td>';
                }
            }
        }

        private getNumberOfDays(year, month) {
            let date = new Date(year, month);
            return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
        }

        private getFirstDay(year, month) {
            let date = new Date(year, month);
            return new Date(`${date.getFullYear()}-${date.getMonth() + 1}-01`).getDay();
        }

    }

    animations = new class animations {

        elements: any = {};

        init() {
            this.addListener();
            this.reloadElements();
            this.makeAnimation();
        }

        reloadElements() {
            this.elements = document.querySelectorAll('[anim]');
            this.elements.forEach(function (item: any, i: number) {
                item.classList.remove('anim-run');
            });
        }

        private addListener() {
            window.addEventListener('scroll', e => {
                this.makeAnimation();
            });
        }

        private makeAnimation() {
            if (this.elements.length) {
                this.elements.forEach(function (item, i) {
                    if (!item.classList.contains('anim-run')) {
                        if (window.scrollY > (item.offsetTop - window.innerHeight)) {
                            let cooldown = typeof item.attributes['anim-cooldown'] != 'undefined' ? parseInt(item.attributes['anim-cooldown'].value) : 100;
                            item.classList.add('anim-prepare');
                            setTimeout(function () {
                                item.classList.remove('anim-prepare');
                                item.classList.add('anim-run');
                            }, cooldown);
                        }
                    }
                });
            }
        }

    }

    parallax = new class parallax {

        elements: any = {};

        init() {
            this.addListener();
            this.reloadElements();
        }

        reloadElements() {
            this.elements = document.querySelectorAll('.parallax');
        }

        private addListener() {
            window.addEventListener('scroll', e => {
                if (this.elements.length) {
                    this.elements.forEach(function (item, i) {
                        let s = typeof item.attributes['parallax-speed'] != 'undefined' ? parseFloat(item.attributes['parallax-speed'].value) : 1;
                        let y = (window.pageYOffset * s) + (typeof item.attributes['parallax-top'] != 'undefined' ? parseFloat(item.attributes['parallax-top'].value) : 0);
                        item.style.backgroundPosition = '0 ' + y + 'px';
                    });
                }
            });
        }

    }

    theme = new class theme {

        constructor() {
            this.addClass();
        }

        private addClass() {
            let cookies = document.cookie.split('; ');
            cookies.forEach(function (item: string, i: number) {
                if (item.lastIndexOf('theme=', 0) === 0) {
                    let theme = item.split('theme=')[1];
                    document.body.classList.add(theme + '-theme');
                }
            });
        }

        private setCookie(theme = 'light', days = 30) {
            let date = new Date(),
                expire = '';
            date.setTime(date.getTime() + (1000 * 60 * 60 * 24 * days));
            expire = '; expires=' + date.toUTCString();
            document.cookie = 'theme=' + theme + expire + ';SameSite=Lax';
        }

        setTheme(theme = 'light') {
            let body = document.body;
            if (theme == 'dark') {
                body.classList.remove('light-theme');
                body.classList.add('dark-theme');
            } else if (theme == 'light') {
                body.classList.add('light-theme');
                body.classList.remove('dark-theme');
            }
            this.setCookie(theme);
        }

    }

    mouseFollower = new class mouseFollower {

        init() {
            this.mouseMoveListener();
            this.addElements();
            this.clickListeners();
        }

        private addElements() {
            let mf = document.createElement('div');
            let mfR = document.createElement('div');
            mf.className = 'mouse-follower';
            mfR.className = 'mouse-follower';
            mf.id = 'mouse-follower';
            mfR.id = 'mouse-follower-ring';
            document.body.prepend(mf);
            document.body.prepend(mfR);
        }

        private clickListeners() {
            window.addEventListener('mousedown', e => {
                let elR = document.getElementById('mouse-follower-ring');
                elR.classList.add('click');
            });
            window.addEventListener('mouseup', e => {
                let elR = document.getElementById('mouse-follower-ring');
                elR.classList.remove('click');
            });
        }

        private mouseMoveListener() {
            window.addEventListener('mousemove', e => {
                let target = e.target as HTMLAreaElement;
                let el = document.getElementById('mouse-follower');
                let elR = document.getElementById('mouse-follower-ring');
                setTimeout(function () {
                    el.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
                }, 25);
                setTimeout(function () {
                    elR.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
                }, 100);
                let tag = target.tagName;
                if (tag == 'BUTTON' || tag == 'A') {
                    if (!(typeof target.attributes['href'] !== 'undefined' && target.attributes['href'].value == '#')) {
                        el.classList.add('grow');
                        elR.classList.add('grow');
                    }
                } else {
                    el.classList.remove('grow');
                    elR.classList.remove('grow');
                }
            });
        }
    }

}

const TUiKit = new TrojanskyUiKit();

