# Trojansky - UI Kit

Required bootstrap 5.1 or newer

Demo: [https://demo.trojansky.pl/tuikit/](https://demo.trojansky.pl/tuikit/)

## Features:

- MouseFollower - Adds an element on the page that follows the mouse cursor.<br>To add a feature
  use `TUiKit.mouseFollower.init();` <br>Element reacts to mouse clicks and hovering over elements:
    - `<a>`
    - `<button>`
- DatePicker - A simple calendar for input fields that have a `datepicker` class.<br>To add a feature
  use `TUiKit.datePicker.init('Your-Date-Format');`
- Animations - Visit the demo page for instructions for use.<br>To enable feature use `TUiKit.animations.init();`
- Parallax - A simple parallax effect.<br>To enable feature use `TUiKit.parallax.init();` and add class `parallax` to
  element.
- Dark/Light Theme - To change theme use `TUiKit.theme.setTheme('dark')`
  or `TUiKit.theme.setTheme('light')`

## Example initialization:

```javascript
<script>(function () {
    TUiKit.mouseFollower.init();
    TUiKit.datePicker.init();
    TUiKit.animations.init();
    TUiKit.parallax.init();
})();
</script>
```